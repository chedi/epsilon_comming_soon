#!/bin/bash
 
NAME="epsilon"                              # Name of the application
DJANGODIR=/home/chedi/www/epsilon_comming_soon    # Django project directory
SOCKFILE=/home/chedi/www/epsilon_comming_soon/run/gunicorn.sock # we will communicte using this unix socket
USER=root                                         # the user to run as
GROUP=root                                        # the group to run as
NUM_WORKERS=4                                     # how many worker processes should Gunicorn spawn
DJANGO_SETTINGS_MODULE=epsilon.settings     # which settings file should Django use
DJANGO_WSGI_MODULE=epsilon.wsgi             # WSGI module name
 
echo "Starting $NAME"
 
# Activate the virtual environment
cd $DJANGODIR
source /home/chedi/www/ve/bin/activate 
export DJANGO_SETTINGS_MODULE=$DJANGO_SETTINGS_MODULE
export PYTHONPATH=$DJANGODIR:$PYTHONPATH
# Create the run directory if it doesn't exist
RUNDIR=$(dirname $SOCKFILE)
test -d $RUNDIR || mkdir -p $RUNDIR
 
# Start your Django Unicorn
# Programs meant to be run under supervisor should not daemonize themselves (do not use --daemon)
exec gunicorn ${DJANGO_WSGI_MODULE}:application \
  --name $NAME \
  --workers $NUM_WORKERS \
  --user=$USER --group=$GROUP \
  --log-level=debug \
  -t 60000 \
  --bind=unix:$SOCKFILE

