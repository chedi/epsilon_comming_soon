from django.conf           import settings
from django.contrib        import admin
from django.conf.urls      import url
from django.conf.urls      import include
from django.conf.urls      import patterns

from epsilon_common.views import Index
from epsilon_common.views import Newsletter


admin.autodiscover()

urlpatterns = patterns(
    '',
    url(r'^$', Index.as_view(), name="home"),
    url(r'^newsletter/', Newsletter.as_view(), name="newsletter"),
)

if settings.DEBUG:
    urlpatterns = patterns(
        '',
        url(r'', include('django.contrib.staticfiles.urls')),
        url(r'^media/(?P<path>.*)$', 'django.views.static.serve', {'document_root': settings.MEDIA_ROOT, 'show_indexes': True}),
    ) + urlpatterns
