import os

DEBUG = True
TEMPLATE_DEBUG = DEBUG
PROJECT_ROOT = os.path.join(os.path.dirname(__file__), '../')

ADMINS = (
    ('chedi toueiti', 'chedi.toueiti@gmail.com'),
)

MANAGERS = ADMINS


ALLOWED_HOSTS = []
LANGUAGE_CODE = 'en-us'
TIME_ZONE     = 'America/Chicago'

SITE_ID  = 1
USE_I18N = True
USE_L10N = True
USE_TZ   = True

MEDIA_ROOT  = os.path.abspath(os.path.join(PROJECT_ROOT, 'media'))
STATIC_ROOT = os.path.abspath(os.path.join(PROJECT_ROOT, 'static'))

MEDIA_URL   = '/media/'
STATIC_URL  = '/static/'

STATICFILES_DIRS = (
    os.path.abspath(os.path.join(PROJECT_ROOT, 'common_static')),
)

STATICFILES_FINDERS = (
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
)

SECRET_KEY = ')aemgxlo93$u2ng6yyc6bf(9ocr)6s($x1q3$ynra4eroe7@h9'

TEMPLATE_LOADERS = (
    'django.template.loaders.filesystem.Loader',
    'django.template.loaders.app_directories.Loader',
)

MIDDLEWARE_CLASSES = (
    'django.middleware.common.CommonMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
)

ROOT_URLCONF     = 'epsilon.urls'
WSGI_APPLICATION = 'epsilon.wsgi.application'

TEMPLATE_DIRS = (
    os.path.abspath(os.path.join(PROJECT_ROOT, 'templates')),
)

INSTALLED_APPS = (
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'django.contrib.admin',
    'django.contrib.contenttypes',
    'django.contrib.auth',
    'django.contrib.sessions',

    'epsilon_common',
)

SESSION_SERIALIZER = 'django.contrib.sessions.serializers.JSONSerializer'

LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'filters': {
        'require_debug_false': {
            '()': 'django.utils.log.RequireDebugFalse'
        }
    },
    'handlers': {
        'mail_admins': {
            'level': 'ERROR',
            'filters': ['require_debug_false'],
            'class': 'django.utils.log.AdminEmailHandler'
        }
    },
    'loggers': {
        'django.request': {
            'handlers': ['mail_admins'],
            'level': 'ERROR',
            'propagate': True,
        },
    }
}
