(function($) {
	"use strict";

    function show_stack_info(message) {
        var info_box;
        var modal_overlay;
        if (typeof info_box != "undefined") {
            info_box.pnotify_display();
            return;
        }
        info_box = $.pnotify({
            text   : message.text,
            title  : message.title,
            type   : "info",
            icon   : "picon picon-object-order-raise",
            delay  : 2000,
            history: false,
            stack  : false,
            before_open: function(pnotify) {
                pnotify.css({
                    "top": ($(window).height() / 2) - (pnotify.height() / 2),
                    "left": ($(window).width() / 2) - (pnotify.width() / 2)
                });
                // Make a modal screen overlay.
                if (modal_overlay) modal_overlay.fadeIn("fast");
                else modal_overlay = $("<div />", {
                    "class": "ui-widget-overlay",
                    "css": {
                        "display" : "none",
                        "position": "fixed",
                        "top"     : "0",
                        "bottom"  : "0",
                        "right"   : "0",
                        "left"    : "0"
                }}).appendTo("body").fadeIn("fast");
            },
            before_close: function() {
                modal_overlay.fadeOut("fast");
        }});
    }

	$("#menu-toggle").click(function(e) {
		e.preventDefault();
		if($("#menu-toggle").hasClass("bt-menu-open")){
			$(this).removeClass("bt-menu-open").addClass("bt-menu-close");
			$("#sidebar-wrapper").removeClass("active");
		}else{
			$(this).removeClass("bt-menu-close").addClass("bt-menu-open");
			$("#sidebar-wrapper").addClass("active");
		}
	});

	$(".show-modal").on("click", function(){
		var modalView = $(this).attr("data-modal");
		$("#modal article").hide();
		$("#modal article#modal-"+modalView).show();
		if($("#modal").is(":visible")){
		}else{
			$("#sidebar-wrapper").removeClass("active");
			$("#modal").slideDown("slow");
			$("#modal").addClass("active");
			$("#menu-toggle").hide();
		}
	});

	$("#modal .bt-modal-close").on("click", function() {
		$("#modal").removeClass("active");
		$("#modal").slideUp("slow", function() {
			$("#menu-toggle").show();
			$("#menu-toggle").removeClass("bt-menu-open").addClass("bt-menu-close");
		});
	});

	setInterval(function() {
		var target = new Date("Mar 10 2014 21:15:00 GMT+0200");
		var now = new Date();
		var difference = Math.floor((target.getTime() - now.getTime()) / 1000);

		var seconds = fixIntegers(difference % 60);
		difference  = Math.floor(difference / 60);

		var minutes = fixIntegers(difference % 60);
		difference  = Math.floor(difference / 60);

		var hours   = fixIntegers(difference % 24);
		difference  = Math.floor(difference / 24);

		var days = difference;

		$(".countdown #seconds").html(seconds);
		$(".countdown #seconds").css({top:"-50px",opacity:0});
		$(".countdown #seconds").animate({top:'0px',opacity:1},500);
		$(".countdown #minutes").html(minutes);
		$(".countdown #hours").html(hours);
		$(".countdown #days").html(days);
	}, 1000);

	function fixIntegers(integer) {
		if (integer < 0)
			integer = 0;
		if (integer < 10)
			return "0" + integer;
		return "" + integer;
	}

	$("#owl-carousel").owlCarousel();

	$("#newsletter-submit").on('click', function(e) {
		e.preventDefault();
		$("#newsletter-submit").html("<i class='fa fa-cog fa-spin'></i> SENDING").prop('disabled', true);
		var $newsletter_form = $('#newsletter-form');
		var fields = $newsletter_form.serialize();
		$.ajax({
			type     : "POST",
			url      : "/newsletter/",
			data     : fields,
			dataType : 'json',
			success  : function(response) {
				if (response.status) {
					$("#emailInputNewsletter").text('')
				}
				$("#newsletter-submit").html("<i class='fa fa-check'></i> SUBMIT").prop('disabled', false);
				$('#newsletter-form-response').empty().html(response.html);
			}
		});
		return false;
	});

	$(window).load(function(){
		$('#preloader').fadeOut(800, function() {
			$('body').css('overflow', 'visible');
			$('.animated').each(function() {
				var elem = $(this);
				var animation = elem.data('animation');
				if (!elem.hasClass('visible') && elem.attr('data-animation') !== undefined) {
					if (elem.attr('data-animation-delay') !== undefined) {
						var timeout = elem.data('animation-delay');
						setTimeout(function() {
							elem.addClass(animation + " visible");
						}, timeout);
					} else {
						elem.addClass(elem.data('animation') + " visible");
					}
				}
			});
		});
	});

    var messages = {};

    messages.linux = {
        title: 'Linux',
        text : 'Linux is good for your health.'
    };

    messages.github = {
        title: 'Github',
        text : 'We love git and contribute to the opensource community.'
    };

    messages.bitbucket = {
        title: 'Bitbucket',
        text : 'Sharing code with client privately never been that simple.'
    };

    messages.android = {
        title: 'Android',
        text : 'You want android mobile integration, we provide it.'
    };

    messages.google = {
        title: 'Google',
        text : 'We run our infrastructure on Google cloud, so we are as releable as they are on this side of things.'
    };
    
	$("#linux_mini_info"    ).on("click", function(e){ e.preventDefault(); show_stack_info(messages.linux    ); return false;})
	$("#github_mini_info"   ).on("click", function(e){ e.preventDefault(); show_stack_info(messages.github   ); return false;})
	$("#google_mini_info"   ).on("click", function(e){ e.preventDefault(); show_stack_info(messages.google   ); return false;})
	$("#android_mini_info"  ).on("click", function(e){ e.preventDefault(); show_stack_info(messages.android  ); return false;})
	$("#bitbucket_mini_info").on("click", function(e){ e.preventDefault(); show_stack_info(messages.bitbucket); return false;})
}(jQuery));
